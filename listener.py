import time
from cgi import parse_qs
import base64
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA256
from Crypto.Signature import PKCS1_v1_5
from configparser import ConfigParser
import requests

with open("pub.key") as pubkey_file:
    pubkey = pubkey_file.read()

rsa_pubkey = RSA.importKey(pubkey)

config = ConfigParser()
config.read('/etc/exonar_hmac_listener.conf') 

def application(env, start_response):
    time_of_reciept = int(time.time())
    try:
        auth_creds = env['HTTP_AUTHORIZATION'].lstrip("Basic ")
        name, signature = base64.b64decode(bytes(auth_creds, 'utf-8')).decode('utf-8').split(":", 1)
        signature = base64.b64decode(bytes(signature, 'utf-8'))
        time_nonce = env['HTTP_X_HMAC_TIME_NONCE']
        time_int = int(time_nonce)
        query_string = env.get('QUERY_STRING', '')
        content_length = int(env.get('CONTENT_LENGTH', "0"))
        if content_length > 0 :
            body = env['wsgi.input'].read(content_length)
        else:
            body = b""
    except (ValueError, KeyError) as e:
        resp_content = "Key Missing"
        resp_code = "401 DENIED"
    else:
        #Aged request
        if time_of_reciept - 20 > time_int:
            resp_content = b"Aged Request"
            resp_code = "401 DENIED"
    
        #Request in future
        elif time_int > time_of_reciept:
            resp_content = b"Future Request"
            resp_code = "401 DENIED"

        #time not aged, but older than a previous request
        elif time_int <= application.last_accept_time:
            resp_content = b"replay"
            resp_code = "401 DENIED"

        else:
            application.last_accept_time = time_int
            target_hash = SHA256.new(bytes(name, 'utf-8'))
            target_hash.update(bytes(time_nonce, 'utf-8'))
            target_hash.update(body)
            target_hash.update(bytes(query_string, 'utf-8'))
            verifier = PKCS1_v1_5.new(rsa_pubkey)
            if 1 == verifier.verify(target_hash, signature):
                response = requests.request(
                        env['REQUEST_METHOD'],
                        config['source']["addr"],
                        params=parse_qs(query_string),
                        data=body
                    )
                resp_code = str(response.status_code)
                resp_content = response.content
            else:
                resp_code = "401 DENIED"
                resp_content = b"Bad Hash"
    start_response(resp_code, [('Content-Type','text/plain'), ('Content-Length', str(len(resp_content)))])
    return [resp_content]

application.last_accept_time = int(time.time()) - 20
