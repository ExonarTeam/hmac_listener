#Exonar HMAC Proxy

This mini-tool listens for a HMAC request and forwards the request content to another server. It should be noted that if the requester has a SSL certificate, SSL Client Certificate Verification is preferable.

#Manual Deployment

This guide presumes CentOS 7

##Obtain Python 3

As a superuser:
```
yum install -y https://centos7.iuscommunity.org/ius-release.rpm
yum update
yum install 
yum install -y python36u python36u-pip python36u-devel gcc
pip3.6 install uwsgi requests
yum remove -y gcc python36u-pip
```

Place the public key of the portal server on the local filesystem with appropriate read permissions.

Add the appropriate values to the `skeleton.conf` file in this repo:

 * `keyfile` should be the location of the public key for the server you are authorising
 * `addr` should be set to the location of the server to which you are forwarding

Place the populated file at `/etc/exonar_hmac_listener.conf`.

#Generating the Public/Private Keypair

The public/private keypair should be generated using the command `ssh-keygen -m PEM`

#Initialising

The server is initialised by the command:

`uwsgi --http :<port> --wsgi-file /path/to/listener.py`

when exposed, when behind a real server use:

`uwsgi --http-socket :<port> --wsgi-file /path/to/listener.py`

#Sending Requests

For testing and validation, use the `test_requester.py` script in this repo.

Requests should be sent with a couple of extra headers:

  * `x-hmac-time-nonce` should be set to the unix timestamp of the request
  * `Authorization` should be set to:
    - "`Basic `"
    - The base64 encoding of:
        + An Arbitrary name identifier, which may not contain a colon
        + Followed by a colon
        + Followed by an RSA-signature based on the SHA256 hash of:
            * The arbitrary name
            * The time nonc
            * The query string
            * The content body 
