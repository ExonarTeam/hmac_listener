#!/usr/bin/env python3
import base64
import requests
import time
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA256
from Crypto import Random
from Crypto.Signature import PKCS1_v1_5
from urllib.parse import urlencode

with open("private.key") as pk_file:
    pk = pk_file.read()

rsa_key = RSA.importKey(pk)
cipher = PKCS1_v1_5.new(rsa_key)

def signature(name, time_nonce, content, query_string):
    target_hash = SHA256.new(bytes(name, 'utf-8'))
    target_hash.update(bytes(time_nonce, 'utf-8'))
    target_hash.update(bytes(content, 'utf-8'))
    target_hash.update(bytes(query_string, 'utf-8'))
    return cipher.sign(target_hash)
        

if __name__ == '__main__':
    #Well formed request
    name = "this_is_my_name" 
    time_nonce = str(int(time.time()))
    content = 'banana'
    data = {'a':'b', 'a':'b'}
    query_string = urlencode(data) 
    signature_val = signature(name, time_nonce, content, query_string)
    headers = {
        'x-hmac-time-nonce': time_nonce,
        'Authorization': b"Basic " + base64.b64encode(bytes(name + ':', 'utf-8') + base64.b64encode(signature_val))
    } 
    response = requests.get("http://localhost:8080", headers=headers, data=content, params=data)
    if 200 == response.status_code:
        print("Normative Response Success")
    else:
        print("Status code was {0}".format(response.status_code))
        print("Content was {0}".format(response.content))
        raise Exception()

    #Time not included
    headers = {
        'Authorization': b"Basic " + base64.b64encode(bytes(name + ':', 'utf-8') + base64.b64encode(signature_val))
    } 
    response = requests.get("http://localhost:8080", headers=headers)
    if 401 == response.status_code:
        print("Time missing causes denied")
    else:
        raise Exception()
    
    #Time is an alpha string
    name = "this_is_my_name" 
    time_nonce = "These are indeed strange times."
    content = ''
    signature_val = signature(name, time_nonce, content, query_string)
    headers = {
        'x-hmac-time-nonce': time_nonce,
        'Authorization': b"Basic " + base64.b64encode(bytes(name + ':', 'utf-8') + base64.b64encode(signature_val))
    } 
    response = requests.get("http://localhost:8080", headers=headers)
    if 401 == response.status_code:
        print("Time is alpha string causes denied")
    else:
        raise Exception()

    #Time is longer than 20 seconds ago
    name = "this_is_my_name" 
    time_nonce = str(int(time.time())-21)
    content = ''
    signature_val = signature(name, time_nonce, content, query_string)
    headers = {
        'x-hmac-time-nonce': time_nonce,
        'Authorization': b"Basic " + base64.b64encode(bytes(name + ':', 'utf-8') + base64.b64encode(signature_val))
    } 
    response = requests.get("http://localhost:8080", headers=headers)

    if 401 == response.status_code:
        print("Aged request is denied.")
    else:
        print("Status code was {0}".format(response.status_code))
        raise Exception()

    #Time is in the future...
    name = "this_is_my_name" 
    time_nonce = str(int(time.time())+21)
    content = ''
    signature_val = signature(name, time_nonce, content, query_string)
    headers = {
        'x-hmac-time-nonce': time_nonce,
        'Authorization': b"Basic " + base64.b64encode(bytes(name + ':', 'utf-8') + base64.b64encode(signature_val))
    } 
    response = requests.get("http://localhost:8080", headers=headers)
    if 401 == response.status_code:
        print("Future request is denied.")
    else:
        print("Status code was {0}".format(response.status_code))
        raise Exception()

    #replay
    name = "this_is_my_name" 
    time_nonce = str(int(time.time()))
    content = ''
    signature_val = signature(name, time_nonce, content, query_string)
    headers = {
        'x-hmac-time-nonce': time_nonce,
        'Authorization': b"Basic" + base64.b64encode(bytes(name + ':', 'utf-8') + base64.b64encode(signature_val))
    } 
    dud_response = requests.get("http://localhost:8080", headers=headers)
    response = requests.get("http://localhost:8080", headers=headers)
    if 401 == response.status_code:
        print("Replay Attack Failed")
    else:
        print("Status code was {0}".format(response.status_code))
        print("Content was {0}".format(response.content))
        raise Exception()
